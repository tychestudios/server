package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import model.CustomerInfo;
import model.Order;

import server.ServletContextClass;

public class DBOps {
	Connection conn = null;
	public String getVendorGCMKey(){
		Statement stmt = null;
		ResultSet rs = null;
		String gcmkey= null;
		try{
			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "select gcmkey from vendor where phonenumber = \"9450132900\"";
			System.out.println(sql);
			rs  = stmt.executeQuery(sql);
			gcmkey  = (rs.next())?rs.getString(1) : null;;
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment

			stmt.close();
			conn.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}
		return gcmkey;

	}

	public String getCustGcmKey(String mobile){
		Statement stmt = null;
		ResultSet rs = null;
		String gcmkey= null;
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "select gcmkey from CUSTOMER_INFO where phonenumber = \""+mobile+"\"";
			System.out.println(sql);
			rs  = stmt.executeQuery(sql);
			gcmkey  = (rs.next())?rs.getString(1) : null;;
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment

			stmt.close();
			conn.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}
		return gcmkey;


	}
	
	public String getToken(String mobile){
		Statement stmt = null;
		ResultSet rs = null;
		String token= null;
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "select OTP from ORDERS where phonenumber = \""+mobile+"\"";
			System.out.println(sql);
			rs  = stmt.executeQuery(sql);
			token  = (rs.next())?rs.getString(1) : null;;
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment

			stmt.close();
			conn.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}
		return token;


	}

	public void addToOrder(Order order,int token){
		Statement stmt = null;
		String gcmkey= null;
		String uniqueId = UUID.randomUUID().toString();
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "insert into ORDERS (orderId, name, phoneNumber, quantity, locality, cost, address, orderStatus, OTP) values(\""+ uniqueId + "\", \"" + order.getName() + "\", \""+ order.getPhoneNumber() + "\", \""+ order.getQuantity() + "\", \""+ order.getLocality() + "\", \""+ order.getCost() + "\", \""+ order.getAddress() + "\", \""+ order.getOrderStatus() + "\","+token+")";
			System.out.println(sql);
			stmt.execute(sql);
			System.out.println("Order query executed..... Inserted in database!!");
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment

			stmt.close();
			conn.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}
	}
	public Boolean checkPrevOrder(String phoneNumber){
		Statement stmt = null;
		String gcmkey= null;
		ResultSet rs = null;
		String method = "checkPrevOrder :: ";
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "select orderstatus from ORDERS where phoneNumber=\"" + phoneNumber+"\" and (orderstatus=\"REQUESTED\" or orderstatus = \"CONFIRMED\")";
			System.out.println(sql);
			rs = stmt.executeQuery(sql);
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment
			if( rs != null && rs.next()) {
				stmt.close();
				conn.close();
				System.out.println(method + "Returning true");
				return true;
			} else{
				stmt.close();
				conn.close();
				System.out.println(method + "Returning false");
				return false;
			}
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
			System.out.println(method + "Returning false");
			return false;
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
			System.out.println(method + "Returning false");
			return false;
		}finally{
			//finally block used to close resources

			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do

			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}		
	}

	public CustomerInfo checkUserExists(String phoneNumber){
		Statement stmt = null;
		String gcmkey= null;
		ResultSet rs = null;
		String method = "checkUserExists :: ";
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "select * from CUSTOMER_INFO where phoneNumber=\"" + phoneNumber+"\"";
			System.out.println(sql);
			rs = stmt.executeQuery(sql);
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment
			if( rs != null && rs.next()) {
	
				
				System.out.println("User exists. Returning CustomerInfo object.");
				return new CustomerInfo(rs.getString("name"),rs.getString("address1"),rs.getString("phoneNumber"),rs.getString("locality"),"",rs.getString("gcmkey"));
			} else{
				stmt.close();
				conn.close();
				System.out.println("Returning null");
				return null;
			}
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
			return null;

		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
			return null;
		}		
	}

	public boolean confirmOrder(String phoneNumber, String orderStatus){
		Statement stmt = null;
		String gcmkey= null;
		ResultSet rs = null;
		String method = "confirmOrder :: ";
		int r = -1;
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "update ORDERS set orderstatus = \"" + orderStatus + "\" where phoneNumber=\"" + phoneNumber+"\" and orderstatus=\"REQUESTED\"";
			System.out.println(sql);
			r = stmt.executeUpdate(sql);
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment
			if(r != -1) {
				stmt.close();
				conn.close();
				System.out.println(method + "Returning true");
				return true;
			} else{
				stmt.close();
				conn.close();
				System.out.println(method + "Returning false");
				return false;
			}
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
			System.out.println(method + "Returning false");
			return false;
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
			System.out.println(method + "Returning false");
			return false;
		}finally{
			//finally block used to close resources

			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do

			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}	
	}

	public boolean updateCustomer(CustomerInfo customer){
		Statement stmt = null;
		String method = "updateCustomer :: ";
		int r = -1;
		try{

			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			String sql;
			sql = "update CUSTOMER_INFO set address1 = \"" + customer.getAddress() + "\",gcmkey= \""+customer.getGcmkey()+"\" where phoneNumber=\"" + customer.getMobile()+"\"";
			System.out.println(sql);
			r = stmt.executeUpdate(sql);
			//address = rs.getString("ADDRESS1");
			//STEP 6: Clean-up environment
			if(r != -1) {
				stmt.close();
				conn.close();
				System.out.println(method + "Returning true");
				return true;
			} else{
				stmt.close();
				conn.close();
				System.out.println(method + "Returning false");
				return false;
			}
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
			System.out.println(method + "Returning false");
			return false;
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
			System.out.println(method + "Returning false");
			return false;
		}finally{
			//finally block used to close resources

			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do

			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			System.out.println("Goodbye!");
		}		
	}
}