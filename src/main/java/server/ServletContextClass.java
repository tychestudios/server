package server;

import java.beans.PropertyVetoException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class ServletContextClass implements ServletContextListener
{
	public static ComboPooledDataSource cpds;

	public void contextInitialized(ServletContextEvent arg0) 
	{
		cpds = new ComboPooledDataSource();
		try {
			cpds.setDriverClass( "com.mysql.jdbc.Driver" );
		} catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //loads the jdbc driver
		cpds.setJdbcUrl( "jdbc:mysql://" + System.getenv().get("OPENSHIFT_MYSQL_DB_HOST") + ":"
				+ System.getenv().get("OPENSHIFT_MYSQL_DB_PORT") + "/waterapp" );
		cpds.setUser("adminQyInWqu");
		cpds.setPassword("y45dyru62Apv");

		cpds.setMinPoolSize(100);
		cpds.setInitialPoolSize(100);
		cpds.setAcquireIncrement(50);
		cpds.setMaxPoolSize(2000);
	}//end contextInitialized method

	public void contextDestroyed(ServletContextEvent arg0) 
	{
		cpds.close ();       
	}//end constextDestroyed method

}
