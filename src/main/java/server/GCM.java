package server;

import java.io.IOException;

import model.Order;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;

public class GCM {
	public static Result sendPushMessage(String registrationId, Order newOrder, int token){
		Sender sender = new Sender("AIzaSyDuVZQNeAESK1HMHWNf-yEI4DotexILoZs");
		Gson gson = new Gson();
		String orderJson = gson.toJson(newOrder);
		Message message = new Message.Builder()
		    .addData("message", orderJson)
		    .addData("token", Integer.toString(token))
		    .build();
		Result result=null;
		int quantity = Integer.parseInt(newOrder.getQuantity() );
		try {
			result = sender.send(message, registrationId, quantity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
		
	}

	public static Result sendPushMessageVendor(String registrationId, Order newOrder){
		Sender sender = new Sender("AIzaSyAyaVKccJzbZESHpraAwxthUnfITSvT7QM");
		Gson gson = new Gson();
		String orderJson = gson.toJson(newOrder);
		Message message = new Message.Builder()
	    .addData("message", orderJson)
	    .build();
		Result result=null;
		try {
			result = sender.send(message, registrationId, 5);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
		
	}
}
