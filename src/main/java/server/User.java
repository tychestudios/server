package server;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import model.CustomerInfo;
import model.Order;

import com.google.android.gcm.server.Result;
import com.google.gson.Gson;

import database.DBOps;

@Path("/")
public class User {

	@POST
	@Path("/addOrder")
	public Response addOrder(@FormParam("order") String nOrder) {

		Gson gson = new Gson();
		Result result = null ;
		Order newOrder = gson.fromJson(nOrder, Order.class);
		System.out.println("Order is :" + nOrder);
		DBOps dbops = new DBOps();
		if(dbops.checkPrevOrder(newOrder.getPhoneNumber()) == false){
			//generate a 4 digit integer 1000 <10000
	        int randomPIN = (int)(Math.random()*9000)+1000;
			System.out.println("Inserting order n db");
			dbops.addToOrder(newOrder,randomPIN);
			System.out.println("order inserted in db");
			String vendorgcmkey = dbops.getVendorGCMKey();
			System.out.println("GCMKey in db :: " + vendorgcmkey);
			result = GCM.sendPushMessageVendor(vendorgcmkey, newOrder) ;
			System.out.println("GCM result is" + result.toString());
			return Response.status(200).entity("Success").build();
		} else{
			return Response.status(200).entity("Order already Requested").build();
		}
	}

	@POST
	@Path("/vendorResponse")
	public Response vendorResponse(@FormParam("order") String nOrder) {
		String method = "vendorResponse :: ";
		System.out.println(nOrder.toString());
		Result result = null ;
		Gson gson = new Gson();
		Order orderResponse = gson.fromJson(nOrder, Order.class);
		DBOps dbops = new DBOps();
		System.out.println(method + "updating the order status to CONFIRMED");
		dbops.confirmOrder(orderResponse.getPhoneNumber(), orderResponse.getOrderStatus());
		System.out.println(method + "UPDATED ORDERSTATUS");
		String custgcmkey = dbops.getCustGcmKey(orderResponse.getPhoneNumber());
		System.out.println("GCMKey in db :: " + custgcmkey);
		int token =Integer.parseInt((dbops.getToken(orderResponse.getPhoneNumber())));
		result = GCM.sendPushMessage(custgcmkey, orderResponse,token) ;
		System.out.println("GCM result is" + result.toString());

		return Response.status(200).entity("Success").build();
	}

	@POST
	@Path("/register")
	public String register(@FormParam("order") String customerInfo) {

		Gson gson = new Gson();
		DBOps db = new DBOps();
		CustomerInfo newCustomer = gson.fromJson(customerInfo, CustomerInfo.class);
		System.out.println(newCustomer.toString());
		if(newCustomer.getName() != null && newCustomer.getMobile() != null && newCustomer.getAddress() != null && newCustomer.getLocality() != null)
		{	System.out.println("Checking if user exists.");
		CustomerInfo customer = db.checkUserExists(newCustomer.getMobile()) ;
		if(customer != null){
			db.updateCustomer(newCustomer);
			return gson.toJson(newCustomer);	
		}
		else{

			Connection conn = null;
			Statement stmt = null;
			try{

				conn = ServletContextClass.cpds.getConnection();

				System.out.println("Creating statement...");
				stmt = (Statement) conn.createStatement();
				String sql;
				sql = "insert into CUSTOMER_INFO values ('"+newCustomer.getName()+"','"+newCustomer.getAddress()+"','"+newCustomer.getMobile()+"','"+newCustomer.getLocality()+"',CURRENT_TIMESTAMP,'"+newCustomer.getGcmkey()+"')";
				System.out.println(sql);
				stmt.execute(sql) ;
				stmt.close();
				conn.close();
				return "REGISTERED";

			}catch(SQLException se){

				se.printStackTrace();
				return "FAILED" ;
			}catch(Exception e){

				e.printStackTrace();
				return "FAILED" ;
			}finally{

				try{
					if(stmt!=null)
						stmt.close();
				}catch(SQLException se2){
				}// nothing we can do
				try{
					if(conn!=null)
						conn.close();
				}catch(SQLException se){
					se.printStackTrace();
				}//end finally try
				System.out.println("Goodbye!");
			}
		}} else {
			return "FAILED" ;
		}
	}

	@POST
	@Path("/getAddress")
	public String getAddress(@FormParam("phoneNumber") String phoneNumber) {
		System.out.println(phoneNumber);
		String address = null;
		if(phoneNumber != null){

			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try{
				System.out.println("Connecting to database...");
				conn = ServletContextClass.cpds.getConnection();

				//STEP 4: Execute a query
				System.out.println("Creating statement...");
				stmt = (Statement) conn.createStatement();
				String sql;
				sql = "SELECT ADDRESS1 FROM CUSTOMER_INFO WHERE PHONENUMBER ='"+phoneNumber+"'";
				System.out.println(sql);
				rs = stmt.executeQuery(sql) ;
				address = rs.getString("ADDRESS1");
				//STEP 6: Clean-up environment
				stmt.close();
				conn.close();
			}catch(SQLException se){
				//Handle errors for JDBC
				se.printStackTrace();
			}catch(Exception e){
				//Handle errors for Class.forName
				e.printStackTrace();
			}finally{
				//finally block used to close resources
				try{
					if(stmt!=null)
						stmt.close();
				}catch(SQLException se2){
				}// nothing we can do
				try{
					if(conn!=null)
						conn.close();
				}catch(SQLException se){
					se.printStackTrace();
				}//end finally try
				System.out.println("Goodbye!");
			}
		}
		return address;
	}

	@GET
	@Path("/completeOrders")
	public String completeOrders() {
		Connection conn;
		Statement stmt;
		try{
			System.out.println("Connecting to database...");
			conn = ServletContextClass.cpds.getConnection();

			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();
			
			String sql;
			sql = "update ORDERS set orderstatus= \"COMPLETED\"";
			System.out.println(sql);
			stmt.executeUpdate(sql) ;
			
			//STEP 6: Clean-up environment
			stmt.close();
			conn.close();
			return "All orders completed";
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}
		return "Something went wrong";
}



	@POST
	@Path("/addNewAddress")
	public String addNewAddress(@FormParam("newAddress") String request) {
		System.out.println(request);
		String address = null;
		if(request != null){
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try{

				//STEP 3: Open a connection
				System.out.println("Connecting to database...");
				conn = ServletContextClass.cpds.getConnection();

				//STEP 4: Execute a query
				System.out.println("Creating statement...");
				stmt = (Statement) conn.createStatement();
				String sql;
				sql = "SELECT ADDRESS1 FROM CUSTOMER_INFO WHERE PHONENUMBER ='"+request+"'";
				System.out.println(sql);
				rs = stmt.executeQuery(sql) ;
				address = rs.getString("ADDRESS1");
				//STEP 6: Clean-up environment
				stmt.close();
				conn.close();
			}catch(SQLException se){
				//Handle errors for JDBC
				se.printStackTrace();
			}catch(Exception e){
				//Handle errors for Class.forName
				e.printStackTrace();
			}finally{
				//finally block used to close resources
				try{
					if(stmt!=null)
						stmt.close();
				}catch(SQLException se2){
				}// nothing we can do
				try{
					if(conn!=null)
						conn.close();
				}catch(SQLException se){
					se.printStackTrace();
				}//end finally try
				System.out.println("Goodbye!");
			}
		}
		return address;
	}



	@POST
	@Path("/vendorRegistration")
	public String vendorRegistration(@FormParam("newgcm") String newgcm) {
		System.out.println(newgcm);
		String address = null;
		if(newgcm != null){
			Connection conn = null;
			Statement stmt = null;
			//ResultSet rs = null;
			try{
				System.out.println("Connecting to database...");
				conn = ServletContextClass.cpds.getConnection();

				//STEP 4: Execute a query
				System.out.println("Creating statement...");
				stmt = (Statement) conn.createStatement();
				String sql;
				sql = "update vendor set gcmkey = \""+newgcm+"\" where phonenumber = \"9450132900\"";
				System.out.println(sql);
				stmt.execute(sql) ;
				//address = rs.getString("ADDRESS1");
				//STEP 6: Clean-up environment
				stmt.close();
				conn.close();
			}catch(SQLException se){
				//Handle errors for JDBC
				se.printStackTrace();
			}catch(Exception e){
				//Handle errors for Class.forName
				e.printStackTrace();
			}finally{
				//finally block used to close resources
				try{
					if(stmt!=null)
						stmt.close();
				}catch(SQLException se2){
				}// nothing we can do
				try{
					if(conn!=null)
						conn.close();
				}catch(SQLException se){
					se.printStackTrace();
				}//end finally try
				System.out.println("Goodbye!");
			}
		}
		return address;
	}

}