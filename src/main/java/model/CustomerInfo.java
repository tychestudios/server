package model;

public class CustomerInfo {

	private String name ;
	private String address ;
	private String mobile ;
	private String locality ;
	private String time_of_order ;
	private String gcmkey ;
	
	
	public String getGcmkey() {
		return gcmkey;
	}
	public void setGcmkey(String gcmkey) {
		this.gcmkey = gcmkey;
	}
	
	
	
	public CustomerInfo(String name, String address, String mobile,
			String locality, String time_of_order,String gcmkey) {
		super();
		this.name = name;
		this.address = address;
		this.mobile = mobile;
		this.locality = locality;
		this.time_of_order = time_of_order;
		this.gcmkey = gcmkey ;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getTime_of_order() {
		return time_of_order;
	}
	public void setTime_of_order(String time_of_order) {
		this.time_of_order = time_of_order;
	}
	
}
