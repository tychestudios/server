package model;

public class Order {
    //static int nextSeq = 0;
	//String orderId;
    @Override
	public String toString() {
		return "Order [name=" + name
				+ ", phoneNumber=" + phoneNumber + ", quantity=" + quantity
				+ ", locality=" + locality + ", cost=" + cost + ", address="
				+ address + ", orderStatus="
				+ orderStatus + "]";
	}

	String name;
    String phoneNumber;
    String quantity;
    String locality;
    String cost;
    String address;
    //String timeStamp;
    String orderStatus;
    
    public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(String name, String phoneNumber, String quantity, String locality, String cost, String address,
			String orderStatus) {
		super();
		//nextSeq++;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.quantity = quantity;
		this.locality = locality;
		this.cost = cost;
		this.address = address;
		this.orderStatus = orderStatus;
	}

	/*public String getOrderId() {
    	this.orderId = String.valueOf(nextSeq);
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

  /*  public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
*/
    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

}